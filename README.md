## **Synopsis** ##

Northwest Baseball WebApplication shows the data of Northwest Baseball sport in a user friendly way so they can search and visualize the data of players and teams.

## **Code Example** ##

This WebApplication deals with the Northwest Baseball sport data using the entity framework. The design for the web application is composed of mapping entities to tables, user interface to interact with the sports data. The application uses SQL Server database in order to store data. The application can be viewed in any major internet browser. It requires connection to Internet in order to launch the application and for the functionalities to work. 

## **Installation** ##

Northwest Baseball WebApplication is built using Microsoft Visual Studio 2015. So, we recommend to follow these steps while getting to the application.

* Download and Install Microsoft Visual Studio 2015 from       [https://www.visualstudio.com/downloads/](Link URL)
* Download and install Microsoft SQL Server 2014 from [https://www.microsoft.com/en-US/download/details.aspx?id=42299](Link URL)
* Update the SQL tools in Visual Studio
* Add the Entity Framework through NuGet Package manager
* Download and install Git from [https://git-scm.com/downloads](Link URL)

### System setup in local machine ###

* Clone the repo
* Open the project in Visual Studio
* Delete migrations folder if any and the old database
* Clean and build the application
* Run the application 

## **Contributing to the project** ##

We welcome you to check the existing issues for bugs or add any issues or enhancements to work on.
[https://bitbucket.org/codeagents7/team03_gdp/issues?status=new&status=open](Link URL)  

## **Who do I talk to?** ##
 
* Abhinaya Mogalipuvvu (E-mail:S525059@mail.nwmissouri.edu)
* Lokesh Mandava (E-mail: S525055@mail.nwmissouri.edu)
* Manindra Naresh Kumar Mamidibathula (E-mail: S525248@mail.nwmissouri.edu)
* Pradeep Kumar Reddy Nandyala (E-mail: S525072@mail.nwmissouri.edu)
* Priyanka Kommula (E-mail: S525033@mail.nwmissouri.edu)
* Thrisha Singi (E-mail: S525114@mail.nwmissouri.edu)
* Veeranjaneyulu Jagarlamudi (E-mail: S525011@mail.nwmissouri.edu)