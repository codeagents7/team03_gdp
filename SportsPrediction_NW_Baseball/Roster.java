import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Roster {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver = new FirefoxDriver();
		driver.get("http://localhost:5000");
		driver.manage().window().maximize();
		
		//Clicking the Roster button
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[3]/a")).click();
		
		//Filter By Player
		driver.get("http://localhost:5000/Roasters");
		driver.findElement(By.id("SearchString")).sendKeys("Fort");
		driver.findElement(By.xpath("html/body/div[2]/div[1]/div[2]/form/input[2]")).click();
		
		//Sorting by FirstName
		driver.get("http://localhost:5000/Roasters");
		driver.findElement(By.xpath("html/body/div[2]/div[1]/div[2]/p[1]/a")).click();
		
		//Sorting by LastName
		driver.get("http://localhost:5000/Roasters");
		driver.findElement(By.xpath("html/body/div[2]/div[1]/div[2]/p[2]/a")).click();
		
		//Sorting by Position
		driver.get("http://localhost:5000/Roasters");
		driver.findElement(By.xpath("html/body/div[2]/div[1]/div[2]/p[3]/a")).click();
		
		//More details about a player
		driver.get("http://localhost:5000/Roasters");
		driver.findElement(By.xpath("html/body/div[2]/div[1]/div[2]/p[3]/a")).click();
		
		//Closing the More details dialog
		driver.get("http://localhost:5000/Roasters");
		driver.findElement(By.xpath("html/body/div[2]/div[1]/div[2]/p[3]/a")).click();
		driver.findElement(By.xpath(".//*[@id='Fort']/div/div/div[3]/button")).click();
	}

}
