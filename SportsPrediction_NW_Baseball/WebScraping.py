from bs4 import BeautifulSoup
import urllib
import os
toOpenUrl=urllib.urlopen("http://bearcatsports.com/cumestats.aspx?path=baseball&year=2015")
urlLink=BeautifulSoup(toOpenUrl, "html.parser")
tables=urlLink.find('table',{"class":"has_sorting stats_table center_wide space_after has_cell_borders"})
table_Body=tables.tbody
dataCmptly=""
for rows in table_Body.findAll('tr'):
    cellInRow=""
    for cells in rows.findAll('td'):
        cellInRow=cellInRow+","+cells.text
    dataCmptly=dataCmptly+"\n"+cellInRow[1:]

print(dataCmptly)
header = "Number,Player FirstName,Player LastName,Avg,gp-gs,ab,r,h,2b,3b,hr,rbi,tb,slg%,bb,hbp,so,gdp,ob%,sf,sh,sb,sba,po,a,e,fld%"+"\n"
file = open(os.path.expanduser("Baseball.csv"), "wb")
file.write(bytes(header))
file.write(bytes(dataCmptly))


