import java.awt.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WebDriver driver = new FirefoxDriver();
		driver.get("http://localhost:5000");
		driver.manage().window().maximize();
		
		//Clicking the Player Batting button
		driver.get("http://localhost:5000");
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[3]/ul/li[1]/a")).click();
		
		/*Clicking the dropdown button
		driver.get("http://localhost:5000");
		WebElement select = driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[3]/ul/li[1]/a"));
		List<WebElement> options = select.findElements(By.tagName("SeasonalStatistics"));
		for (WebElement option : options) {
		    if("PlayerBatting".equals(option.getText()))
		        option.click();   
		}*/
		
		/*driver.get("http://localhost:5000");
		WebElement select1 = driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[3]/ul/li[2]/a"));
		List<WebElement> options = select.findElements(By.tagName("SeasonalStatistics"));
		for (WebElement option : options) {
		    if("PlayerFielding".equals(option.getText()))
		        option.click();   
		}*/
		
		//Typing any player name in the textbox
		driver.findElement(By.id("SearchString")).sendKeys("Fort");
		
		//Clicking the Search button
		driver.findElement(By.xpath("html/body/div[2]/form/p/input[2]")).click();
		
		//Sorting by Player Number
		driver.get("http://localhost:5000/PlayerBattings/Batting");
		driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[1]/th[2]/a")).click();
		
		//Sorting by Name
		driver.get("http://localhost:5000/PlayerBattings/Batting");
		driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[1]/th[1]/a")).click();
		
		//Redirecting to Morestatistics page
		driver.get("http://localhost:5000/PlayerBattings/Batting");
		driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[2]/td[7]/a")).click();
		
		//Checking Back to search button
		driver.get("http://localhost:5000/PlayerBattings/Batting");
		driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[2]/td[7]/a")).click();
		driver.findElement(By.xpath("html/body/div[2]/div[2]/a")).click();
		
		//Checking the Right Carousel arrow
		driver.get("http://localhost:5000");
		driver.findElement(By.xpath(".//*[@id='myCarousel']/a[2]/span[1]")).click();
		driver.findElement(By.xpath(".//*[@id='myCarousel']/a[2]")).click();
		
		//Checking the Left Carousel arrow
		driver.get("http://localhost:5000");
		driver.findElement(By.xpath(".//*[@id='myCarousel']/a[1]/span[1]")).click();
		driver.findElement(By.xpath(".//*[@id='myCarousel']/a[1]")).click();
		
		//Roster button
		driver.get("http://localhost:5000");
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[1]/a")).click();
		
		//Visualization button
		driver.get("http://localhost:5000");
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[2]/a")).click();
		
		//Seasonal Statistics button
		driver.get("http://localhost:5000");
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[3]/a")).click();
		
		//Game Statistics button
		driver.get("http://localhost:5000");
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[4]/a")).click();
		
		//More Info button
		driver.get("http://localhost:5000");
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[5]/a")).click();
		
		//About us page
		driver.findElement(By.xpath("html/body/div[2]/footer/div/a[1]")).click();
				
		//Contact us page
		driver.findElement(By.xpath("html/body/div[2]/footer/div/a[3]")).click();
				
		//FAQs page
		driver.findElement(By.xpath("html/body/div[2]/footer/div/a[2]")).click();
	}

}
