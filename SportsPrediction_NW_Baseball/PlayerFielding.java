import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class PlayerFielding {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver = new FirefoxDriver();
		driver.get("http://localhost:5000");
		driver.manage().window().maximize();
		
		//Clicking the Player Batting button
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[3]/a")).click();
		driver.findElement(By.xpath("html/body/div[1]/div/div[2]/ul[2]/li[3]/ul/li[2]/a")).click();
		
		//Filtering Players by year
		//driver.get("http://localhost:5000/PlayerBattings/Fielding");
		//driver.manage().window().maximize();
		driver.findElement(By.id("year")).click();
		WebElement year = driver.findElement(By.xpath(".//*[@id='year']"));
		List<WebElement> options = year.findElements(By.tagName("option"));
		for (WebElement option : options) {
		    if("2015".equals(option.getText()))
		        option.click(); 
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Sorting
		driver.get("http://localhost:5000/PlayerBattings/Fielding");
		driver.manage().window().maximize();
		driver.findElement(By.id("sortType")).click();
		WebElement sortBy = driver.findElement(By.xpath(".//*[@id='sortType']"));
		List<WebElement> options1 = sortBy.findElements(By.tagName("option"));
		for (WebElement option : options1) {
		    if("PlayerNumber".equals(option.getText()))
		        option.click(); 
		}
		
		//Typing any player name in the textbox
		driver.findElement(By.id("SearchString")).sendKeys("Nick");
		
		//Clicking the Search button
		driver.findElement(By.xpath("html/body/div[2]/form/p/input[2]")).click();
		
		//Sorting by Player Number
		driver.get("http://localhost:5000/PlayerBattings/Fielding");
		driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[1]/th[2]/a")).click();
				
		//Sorting by Name
		driver.get("http://localhost:5000/PlayerBattings/Fielding");
		driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[1]/th[1]/a")).click();
				
		//Redirecting to Morestatistics page
		driver.get("http://localhost:5000/PlayerBattings/Fielding");
		driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[2]/td[7]/a")).click();
		
		//Clicking Back to search button
		driver.get("http://localhost:5000/PlayerBattings/Fielding");
		driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[2]/td[7]/a")).click();
		driver.findElement(By.xpath("html/body/div[2]/div[2]/a")).click();


	}

}
