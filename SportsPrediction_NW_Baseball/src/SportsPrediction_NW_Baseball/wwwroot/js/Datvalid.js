﻿function validatedate(searchString) {
    var flag = false;
    //var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-](200[0-9]|201[0-9]|2020)$/;
    // Match the date format through regular expression
    if (searchString.value.match(dateformat)) {
        //document.form1.text1.focus();
        //Test which seperator is used '/' or '-'
        var opera1 = searchString.value.split('/');
        var opera2 = searchString.value.split('-');
        lopera1 = opera1.length;
        lopera2 = opera2.length;
        // Extract the string into month, date and year
        if (lopera1 > 1) {
            var pdate = searchString.value.split('/');
        }
        else
            if (lopera2 > 1) {
                var pdate = searchString.value.split('-');
            }
        var mm = parseInt(pdate[0]);
        var dd = parseInt(pdate[1]);
        var yy = parseInt(pdate[2]);
        // Create list of days of a month [assume there is no leap year by default]
        var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        if (searchString.length == 0) {
            alert("date cannot be space");
            return false;
        }

        if (mm < 1 || mm > 12) {
            flag = true;
            alert("Invalid date format!mm");
            //document.form1.text1.focus();
            return false;
        }

        if (dd < 1 || dd > 31) {
            flag = true;
            alert("Invalid date format!dd");
            //document.form1.text1.focus();
            return false;
        }

        if (yy > 2016)
        {
            flag = true;
            alert("<p style='margin-left:240px;'>Year should not exceed 2016.<p>");
            return false;
        }

        if (!flag)
        {
            return true;
        }
    }
    else {
        alert("Invalid data format.Please enter mm/dd/yyyy format");
        return false;
    }
}
