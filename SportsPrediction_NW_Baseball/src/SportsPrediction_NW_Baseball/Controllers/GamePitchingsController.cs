// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="GamePitchingsController.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using SportsPrediction_NW_Baseball.Models;
using System;
using System.Collections.Generic;

namespace SportsPrediction_NW_Baseball.Controllers
{
    /// <summary>
    /// Class GamePitchingsController.
    /// </summary>
    /// <seealso cref="Microsoft.AspNet.Mvc.Controller" />
    public class GamePitchingsController : Controller
    {
        /// <summary>
        /// The context
        /// </summary>
        private ApplicationDbContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="GamePitchingsController" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public GamePitchingsController(ApplicationDbContext context)
        {
            _context = context;    
        }


        // GET: search by season,opponent to yield game statistics.
        /// <summary>
        /// Games es the specified search string.
        /// </summary>
        /// <param name="SearchString">The search string.</param>
        /// <param name="year">The year.</param>
        /// <param name="typesel">The typesel.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Games(string SearchString, string year, string typesel)
        {
            ViewBag.SearchString = SearchString;
            ViewBag.bat = null;
            ViewBag.pitch = null;
            ViewBag.pitchs = null;
            ViewBag.nams = null;
            ViewBag.nam = null;

            //generic object
            List<object> mymodel = new List<object>();

            //drop down list populating for years.
            var yearlist = new List<String>();

            var years = from m in _context.GamePitchings
                        orderby m.Season descending
                        select m.Season;

            yearlist.AddRange(years.Distinct());

            ViewBag.year = new SelectList(yearlist);

            //type populating.
            var types = new List<String>();
            types.Add("Pitching");
            types.Add("Batting");

            ViewBag.typesel = new SelectList(types);

            //search operations
            var pitchings = from m in _context.GamePitchings
                            select m;

            var battings = from m in _context.GameBattings
                           select m;

            //search based on the type,year and opponent name is not entered. 
            if (String.IsNullOrEmpty(SearchString) && !String.IsNullOrEmpty(year) && !String.IsNullOrEmpty(typesel))
            {
                if (typesel == "Batting")
                {
                    battings = battings.Where(s => s.Season == year);
                    mymodel.Add(battings);
                    ViewBag.bat = battings;
                    return View(mymodel);
                }

                if (typesel == "Pitching")
                {
                    pitchings = pitchings.Where(s => s.Season == year);
                    mymodel.Add(pitchings);
                    ViewBag.pitch = pitchings;
                    return View(mymodel);
                }
            }

            //search based on the type,year and opponent name selected. 
            if (!String.IsNullOrEmpty(SearchString) || !String.IsNullOrEmpty(year) || !String.IsNullOrEmpty(typesel))
            {
                if (typesel == "Batting")
                {
                    battings = battings.Where(s => (s.Opponent.Contains(SearchString)) && (s.Season == year));
                    var cnt = battings.Count();
                    if (cnt != 0)
                    {
                        mymodel.Add(battings);
                        ViewBag.bat = battings;
                        return View(mymodel);
                    }
                    else
                    {
                        ViewBag.bat = null;
                        ViewBag.nam = "noresult";
                        return View(mymodel);
                    }
                }

                if (typesel == "Pitching")
                {
                    pitchings = pitchings.Where(s => (s.Opponent.Contains(SearchString)) && (s.Season == year));
                    var cnt = pitchings.Count();
                    if (cnt != 0)
                    {
                        mymodel.Add(pitchings);
                        ViewBag.pitch = pitchings;
                        return View(mymodel);
                    }
                    else
                    {
                        ViewBag.pitch = null;
                        ViewBag.nams = "noresult";
                        return View(mymodel);
                    }
                }
            }

            //default data to be displayed on the view initially.
            var pitchingss = from m in _context.GamePitchings
                             select m;

            pitchingss = pitchingss.Where(s => (s.Type == types[0]) && (s.Season == yearlist[0]));

            mymodel.Add(pitchingss);
            ViewBag.pitchs = pitchingss;
            return View(mymodel);
        }

        //populating the statics of the game after clicking on the more statistics.
        /// <summary>
        /// Games the batting stats.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult GameBattingStats(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            GameBatting gamebatting = _context.GameBattings.Single(m => m.GameBattingID == id);
            if (gamebatting == null)
            {
                return HttpNotFound();
            }

            return View(gamebatting);
        }

        //populating the statics of the game after clicking on the more statistics.
        /// <summary>
        /// Games the pitching stats.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult GamePitchingStats(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            GamePitching gamepitching = _context.GamePitchings.Single(m => m.GamePitchingID == id);
            if (gamepitching == null)
            {
                return HttpNotFound();
            }

            return View(gamepitching);
        }


        // GET: Player Pitching view to search by match date and type of statistics also.
        /// <summary>
        /// Games the pitching.
        /// </summary>
        /// <param name="SearchString">The search string.</param>
        /// <param name="typesel">The typesel.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult GamePitching(string SearchString, string typesel)
        {
            ViewBag.SearchString = SearchString;
            ViewBag.nam = null;
            //search operations
            var pitchss = from m in _context.GamePitchings
                          select m;

            var batss = from m in _context.GameBattings
                        select m;

            //type populating.
            var types = new List<string>();
            types.Add("All Statistics");
            types.Add("Pitching");
            types.Add("Batting");

            ViewBag.typesel = new SelectList(types);

            //search based on the date entered.
            if (!String.IsNullOrEmpty(SearchString) && !String.IsNullOrEmpty(typesel))
            {
                var x = SearchString;
                if (x.Length == 10)
                {
                    var d = string.Concat(x.Substring(0, 6), x.Substring(8, 2));


                    if (typesel == "All Statistics")
                    {
                        pitchss = pitchss.Where(s => (s.Matchdate == d));
                        var row = pitchss.Count();

                        batss = batss.Where(s => (s.Matchdate == d));
                        var row1 = batss.Count();

                        if (row != 0 && row1 != 0)
                        {
                            ViewBag.nam = pitchss;
                            ViewBag.bat = batss;
                            return View(pitchss);
                        }
                        else
                        {
                            ViewBag.nam = null;
                            ViewBag.bat = null;
                            ViewBag.nams = "noresult";
                        }
                    }


                    if (typesel == "Pitching")
                    {
                        pitchss = pitchss.Where(s => (s.Matchdate == d));
                        var row = pitchss.Count();

                        if (row != 0)
                        {
                            ViewBag.nam = pitchss;
                            return View(pitchss);
                        }
                        else
                        {
                            ViewBag.nam = null;
                            ViewBag.nams = "noresult";
                        }
                    }

                    if (typesel == "Batting")
                    {
                        batss = batss.Where(s => (s.Matchdate == d));
                        var row = batss.Count();

                        if (row != 0)
                        {
                            ViewBag.bat = batss;
                            return View(batss);
                        }
                        else
                        {
                            ViewBag.bat = null;
                            ViewBag.nams = "noresult";
                        }
                    }
                }
            }
            var fields = from m in _context.GamePitchings
                         select m;

            return View(fields);
        }




        // GET: Player Pitching view to search by match date and opponent name also.
        /// <summary>
        /// Games the pitchings.
        /// </summary>
        /// <param name="SearchString">The search string.</param>
        /// <param name="Oppname">The oppname.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult GamePitchings(string SearchString, string Oppname)
        {
            ViewBag.nam = null;
            //search operations
            var pitchss = from m in _context.GamePitchings
                          select m;

            //search based on the date entered.
            if (!String.IsNullOrEmpty(SearchString) && String.IsNullOrEmpty(Oppname))
            {
                pitchss = pitchss.Where(s => (s.Matchdate == SearchString));
                var row = pitchss.Count();

                if (row != 0)
                {
                    ViewBag.nam = pitchss;
                    return View(pitchss);
                }
                else
                {
                    ViewBag.nam = null;
                    ViewBag.nams = "noresult";
                }
            }

            //search based on the date and the opponent name entered.
            if (!String.IsNullOrEmpty(SearchString) && !String.IsNullOrEmpty(Oppname))
            {
                pitchss = pitchss.Where(s => (s.Matchdate == SearchString) && (s.Opponent.Contains(Oppname)));
                var row = pitchss.Count();

                if (row != 0)
                {
                    ViewBag.nam = pitchss;
                    return View(pitchss);
                }
                else
                {
                    ViewBag.nam = null;
                    ViewBag.nams = "noresult";
                }
            }

            var fields = from m in _context.GamePitchings
                         select m;

            return View(fields);
        }


        // GET: GamePitchings
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Index()
        {
            return View(_context.GamePitchings.ToList());
        }

        // GET: GamePitchings/Details/5
        /// <summary>
        /// Detailses the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            GamePitching gamePitching = _context.GamePitchings.Single(m => m.GamePitchingID == id);
            if (gamePitching == null)
            {
                return HttpNotFound();
            }

            return View(gamePitching);
        }

        // GET: GamePitchings/Create
        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: GamePitchings/Create
        /// <summary>
        /// Creates the specified game pitching.
        /// </summary>
        /// <param name="gamePitching">The game pitching.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(GamePitching gamePitching)
        {
            if (ModelState.IsValid)
            {
                _context.GamePitchings.Add(gamePitching);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gamePitching);
        }




        // GET: GamePitchings/Edit/5
        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            GamePitching gamePitching = _context.GamePitchings.Single(m => m.GamePitchingID == id);
            if (gamePitching == null)
            {
                return HttpNotFound();
            }
            return View(gamePitching);
        }

        // POST: GamePitchings/Edit/5
        /// <summary>
        /// Edits the specified game pitching.
        /// </summary>
        /// <param name="gamePitching">The game pitching.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(GamePitching gamePitching)
        {
            if (ModelState.IsValid)
            {
                _context.Update(gamePitching);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gamePitching);
        }

        // GET: GamePitchings/Delete/5
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            GamePitching gamePitching = _context.GamePitchings.Single(m => m.GamePitchingID == id);
            if (gamePitching == null)
            {
                return HttpNotFound();
            }

            return View(gamePitching);
        }

        // POST: GamePitchings/Delete/5
        /// <summary>
        /// Deletes the confirmed.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            GamePitching gamePitching = _context.GamePitchings.Single(m => m.GamePitchingID == id);
            _context.GamePitchings.Remove(gamePitching);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
