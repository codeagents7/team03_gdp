﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="HomeController.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace SportsPrediction_NW_Baseball.Controllers
{
    /// <summary>
    /// Class HomeController.
    /// </summary>
    /// <seealso cref="Microsoft.AspNet.Mvc.Controller" />
    public class HomeController : Controller
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Abouts this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        /// <summary>
        /// Contacts this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        /// <summary>
        /// Errors this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Error()
        {
            return View();
        }
        /// <summary>
        /// Why the with us.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult whyWithUs()
        {

            return View();
        }
        /// <summary>
        /// FAQs this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult faq()
        {
            return View();
        }
        /// <summary>
        /// Teams this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Team()
        {
            return View();
        }
        /// <summary>
        /// Constructions this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult construction()
        {
            return View();
        }
        /// <summary>
        /// Performances this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Performance()
        {
            return View();
        }
        /// <summary>
        /// Teams the performance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult TeamPerformance()
        {
            return View();
        }
        /// <summary>
        /// Progresses this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Progress()
        {
            return View();
        }
        /// <summary>
        /// Deletes this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Delete()
        {
            return View();
        }
        /// <summary>
        /// Galleries this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult gallery()
        {
            return View();
        }
        /// <summary>
        /// Rules this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Rules()
        {
            return View();
        }
        /// <summary>
        /// Seasons the playerruns.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult SeasonPlayerruns()
        {
            return View();
        }
        /// <summary>
        /// Player runs this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Playerruns()
        {
            return View();
        }
        /// <summary>
        /// Scores this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Scores()
        {
            return View();
        }
        /// <summary>
        /// Generates the visualization.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult generateVisualization()
        {
            return View();
        }
        /// <summary>
        /// Players era.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult PlayersERA()
        {
            return View();
        }
        /// <summary>
        /// NWSU opponents.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult NWSUopponents()
        {
            return View();
        }
        public IActionResult HeatMap()
        {
            return View();
        }
    }
}
