// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="RoastersController.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using SportsPrediction_NW_Baseball.Models;
using System;

namespace SportsPrediction_NW_Baseball.Controllers
{
    /// <summary>
    /// Class RoastersController.
    /// </summary>
    /// <seealso cref="Microsoft.AspNet.Mvc.Controller" />
    public class RoastersController : Controller
    {
        /// <summary>
        /// The context
        /// </summary>
        private ApplicationDbContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoastersController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public RoastersController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Roasters
        /// <summary>
        /// Indexes the specified sort order.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="searchString">The search string.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Index(string sortOrder, string searchString)
        {
            ViewBag.LastNameSortParm = String.IsNullOrEmpty(sortOrder) ? "lastname" : "";
            ViewBag.FirstNameSortParm = String.IsNullOrEmpty(sortOrder) ? "firstname" : "";
            ViewBag.PositionSortParm = String.IsNullOrEmpty(sortOrder) ? "position" : "";
            var roasters = from s in _context.Roaster.ToList()
                             select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                roasters = roasters.Where(s => s.LastName.Contains(searchString)
                                       || s.FirstName.Contains(searchString) || s.Position.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "firstname":
                    roasters = roasters.OrderByDescending(s => s.FirstName);
                    break;
                case "lastname":
                    roasters = roasters.OrderBy(s => s.LastName);
                    break;
                case "position":
                    roasters = roasters.OrderBy(s => s.Position);
                    break;
                default:
                    roasters = roasters.OrderBy(s => s.FirstName);
                    break;
            }
            return View(roasters.ToList());
        }

        // GET: Roasters/Details/5
        /// <summary>
        /// Detailses the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Roaster roaster = _context.Roaster.Single(m => m.RoasterID == id);
            if (roaster == null)
            {
                return HttpNotFound();
            }

            return View(roaster);
        }

        // GET: Roasters/Create
        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: Roasters/Create
        /// <summary>
        /// Creates the specified roaster.
        /// </summary>
        /// <param name="roaster">The roaster.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Roaster roaster)
        {
            if (ModelState.IsValid)
            {
                _context.Roaster.Add(roaster);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(roaster);
        }

        // GET: Roasters/Edit/5
        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Roaster roaster = _context.Roaster.Single(m => m.RoasterID == id);
            if (roaster == null)
            {
                return HttpNotFound();
            }
            return View(roaster);
        }

        // POST: Roasters/Edit/5
        /// <summary>
        /// Edits the specified roaster.
        /// </summary>
        /// <param name="roaster">The roaster.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Roaster roaster)
        {
            if (ModelState.IsValid)
            {
                _context.Update(roaster);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(roaster);
        }

        // GET: Roasters/Delete/5
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Roaster roaster = _context.Roaster.Single(m => m.RoasterID == id);
            if (roaster == null)
            {
                return HttpNotFound();
            }

            return View(roaster);
        }

        // POST: Roasters/Delete/5
        /// <summary>
        /// Deletes the confirmed.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Roaster roaster = _context.Roaster.Single(m => m.RoasterID == id);
            _context.Roaster.Remove(roaster);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
