// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="PlayersController.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using SportsPrediction_NW_Baseball.Models;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;

namespace SportsPrediction_NW_Baseball.Controllers
{
    /// <summary>
    /// Class PlayersController.
    /// </summary>
    /// <seealso cref="Microsoft.AspNet.Mvc.Controller" />
    public class PlayersController : Controller
    {
        /// <summary>
        /// The context
        /// </summary>
        private ApplicationDbContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayersController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public PlayersController(ApplicationDbContext context)
        {
            _context = context;    
        }

        /// <summary>
        /// Indexes the specified search string.
        /// </summary>
        /// <param name="searchString">The search string.</param>
        /// <param name="searchString1">The search string1.</param>
        /// <returns>Task&lt;IActionResult&gt;.</returns>
        public async Task<IActionResult> Index(string searchString, string searchString1)
        {
            var playerName = from m in _context.Player
                             select m;

            if (!(String.IsNullOrEmpty(searchString) || String.IsNullOrEmpty(searchString1)))
            {
                playerName = playerName.Where(s => s.PlayerName.Contains(searchString));
            }
            string json = JsonConvert.SerializeObject(playerName);
            ViewData["jsondata"] = json;
            Console.WriteLine("ufhifhdziooooooooo", json);
            return View(await playerName.ToListAsync());
        }


        // GET: Players/Details/5
        /// <summary>
        /// Detailses the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Player player = _context.Player.Single(m => m.PlayerNumber == id);
            if (player == null)
            {
                return HttpNotFound();
            }

            return View(player);
        }

        // GET: Players/Create
        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: Players/Create
        /// <summary>
        /// Creates the specified player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Player player)
        {
            if (ModelState.IsValid)
            {
                _context.Player.Add(player);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(player);
        }

        // GET: Players/Edit/5
        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Player player = _context.Player.Single(m => m.PlayerNumber == id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        // POST: Players/Edit/5
        /// <summary>
        /// Edits the specified player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Player player)
        {
            if (ModelState.IsValid)
            {
                _context.Update(player);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(player);
        }

        // GET: Players/Delete/5
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Player player = _context.Player.Single(m => m.PlayerNumber == id);
            if (player == null)
            {
                return HttpNotFound();
            }

            return View(player);
        }

        // POST: Players/Delete/5
        /// <summary>
        /// Deletes the confirmed.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Player player = _context.Player.Single(m => m.PlayerNumber == id);
            _context.Player.Remove(player);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
