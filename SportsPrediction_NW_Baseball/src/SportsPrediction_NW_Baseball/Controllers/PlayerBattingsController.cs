// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="PlayerBattingsController.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using SportsPrediction_NW_Baseball.Models;
using System;
using System.Collections.Generic;

namespace SportsPrediction_NW_Baseball.Controllers
{
    /// <summary>
    /// Class PlayerBattingsController.
    /// </summary>
    /// <seealso cref="Microsoft.AspNet.Mvc.Controller" />
    public class PlayerBattingsController : Controller
    {
        /// <summary>
        /// The context
        /// </summary>
        private ApplicationDbContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerBattingsController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public PlayerBattingsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        //// GET: Player Batting view.
        //public IActionResult Battings(string sortOrder, string SearchString, string year)
        //{
        //    //ViewBag.CurrentSort = sortOrder;
        //    //ViewBag.CurrentSort = sortOrder;
        //    ViewBag.playerNumberSort = String.IsNullOrEmpty(sortOrder) ? "player_desc" : "";
        //    ViewBag.nameSort = sortOrder == "Name" ? "name_desc" : "Name";

        //    ViewBag.CurrentFilter = SearchString;

        //    //drop down list populating for year
        //    var yearlist = new List<string>();

        //    var years = from m in _context.PlayerBattings
        //                orderby m.Year descending
        //                select m.Year;

        //    ViewBag.year = new SelectList(years.Distinct());

        //    //search operations
        //    var players = from m in _context.PlayerBattings
        //                  select m;

        //    //var flag = false;
        //    if (!String.IsNullOrEmpty(SearchString))
        //    {
        //        players = players.Where(s => (s.Name.Contains(SearchString) && s.Year == year));
        //        return View(players);
        //    }

        //    if (!String.IsNullOrEmpty(year))
        //    {
        //        players = players.Where(s => (s.Year == year));
        //        return View(players);
        //    }

        //    //sort based on the sort parameter
        //    switch (sortOrder)
        //    {
        //        //var y = year;
        //        case "player_desc":
        //            //players = players.Where(s => (s.Year == year));
        //            players = players.OrderByDescending(s => s.PlayerNumber);
        //            break;
        //        case "Name":
        //            //players = players.Where(s => (s.Year == year));
        //            players = players.OrderBy(s => s.Name);
        //            break;
        //        case "name_desc":
        //            //players = players.Where(s => (s.Year == year));
        //            players = players.OrderByDescending(s => s.Name);
        //            break;
        //        default:
        //            //players = players.Where(s => (s.Year == year));
        //            players = players.OrderBy(s => s.PlayerNumber);
        //            break;
        //    }

        //    return View(players);
        //}


        // GET: Player Batting view.
        /// <summary>
        /// Battings the specified sort order.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="SearchString">The search string.</param>
        /// <param name="year">The year.</param>
        /// <param name="sortType">Type of the sort.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Batting(string sortOrder, string SearchString, string year, string sortType)
        {
            ViewBag.noresult = null;
            ViewBag.playerNumberSort = String.IsNullOrEmpty(sortOrder) ? "player_desc" : "";
            ViewBag.nameSort = sortOrder == "Name" ? "name_desc" : "Name";

            //drop down list populating for year
            var yearlist = new List<string>();

            var years = from m in _context.PlayerBattings
                        orderby m.Year descending
                        select m.Year;

            yearlist.AddRange(years.Distinct());
            ViewBag.year = new SelectList(yearlist);

            //drop down list populating for sortType
            var sortlist = new List<string>();

            sortlist.Add("PlayerNumber");
            //sortlist.Add("Name");
            sortlist.Add("Hits");
            sortlist.Add("HomeRun");
            sortlist.Add("Runs");
           
            ViewBag.sortType = new SelectList(sortlist);

            //search operations
            var players = from m in _context.PlayerBattings
                          select m;

            if (!String.IsNullOrEmpty(SearchString))
            {
                players = players.Where(s => (s.Name.Contains(SearchString) && s.Year == year));
                var count = players.Count();
                if (count != 0)
                {
                    //sort based on the sort parameter
                    switch (sortType)
                    {
                        case "PlayerNumber":
                            players = players.Where(s => (s.Name.Contains(SearchString) && s.Year == year)).OrderBy(s => s.PlayerNumber);
                            break;
                        case "Hits":
                            players = players.Where(s => (s.Name.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.Hits);
                            break;
                        case "HomeRun":
                            players = players.Where(s => (s.Name.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.HomeRun);
                            break;
                        default:
                            players = players.Where(s => (s.Name.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.Runs);
                            break;
                    }
                   
                    return View(players);
                }
                else
                {
                    ViewBag.noresult = "noresult";
                    return View(players);
                }
            }

            if (!String.IsNullOrEmpty(year) && !String.IsNullOrEmpty(sortType))
            {
                switch (sortType)
                {
                    case "PlayerNumber":
                        players = players.Where(s => (s.Year == year)).OrderBy(s => s.PlayerNumber);
                        break;
                    case "Hits":
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.Hits);
                        break;
                    case "HomeRun":
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.HomeRun);
                        break;
                    default:
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.Runs);
                        break;
                }

                return View(players);
            }

            //default data to be displayed on the view initially.
            var battingss = from m in _context.PlayerBattings
                            select m;

            battingss = battingss.Where(s => (s.Year == yearlist[0])).OrderBy(s=> s.PlayerNumber);
            ViewBag.defualt = battingss;
            return View(battingss);
        }


        /// <summary>
        /// Fieldings the specified sort order.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="SearchString">The search string.</param>
        /// <param name="year">The year.</param>
        /// <param name="sortType">Type of the sort.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Fielding(string sortOrder, string SearchString, string year, string sortType)
        {
            ViewBag.noresult = null;

            //drop down list populating for year
            var yearlist = new List<string>();

            var years = from m in _context.PlayerFieldings
                        orderby m.Year descending
                        select m.Year;

            yearlist.AddRange(years.Distinct());
            ViewBag.year = new SelectList(yearlist);

            //drop down list populating for sortType
            var sortlist = new List<string>();

            sortlist.Add("PlayerNumber");
            sortlist.Add("Assist");
            sortlist.Add("DoublePlay");
            sortlist.Add("Putout");

            ViewBag.sortType = new SelectList(sortlist);

            //search operations
            var players = from m in _context.PlayerFieldings
                          select m;

            if (!String.IsNullOrEmpty(SearchString))
            {
                players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year));
                var count = players.Count();
                if (count != 0)
                {
                    //sort based on the sort parameter
                    switch (sortType)
                    {
                        case "PlayerNumber":
                            players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year)).OrderBy(s => s.PlayerNumber);
                            break;
                        case "Assist":
                            players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.Assist);
                            break;
                        case "DoublePlay":
                            players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.DoublePlay);
                            break;
                        default:
                            players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.Putout);
                            break;
                    }

                    return View(players);
                }
                else
                {
                    ViewBag.noresult = "noresult";
                    return View(players);
                }
            }

            if (!String.IsNullOrEmpty(year) && !String.IsNullOrEmpty(sortType))
            {
                switch (sortType)
                {
                    case "PlayerNumber":
                        players = players.Where(s => (s.Year == year)).OrderBy(s => s.PlayerNumber);
                        break;
                    case "Assist":
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.Assist);
                        break;
                    case "DoublePlay":
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.DoublePlay);
                        break;
                    default:
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.Putout);
                        break;
                }

                return View(players);
            }

            // default data to be displayed on the view initially.
            var fieldingss = from m in _context.PlayerFieldings
                             select m;

            fieldingss = fieldingss.Where(s => (s.Year == yearlist[0])).OrderBy(s => s.PlayerNumber);
            ViewBag.defualt = fieldingss;
            return View(fieldingss);
        }

        /// <summary>
        /// Pitchings the specified search string.
        /// </summary>
        /// <param name="SearchString">The search string.</param>
        /// <param name="year">The year.</param>
        /// <param name="sortType">Type of the sort.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Pitching(string SearchString, string year, string sortType)
        {
            ViewBag.noresult = null;

            //drop down list populating for year
            var yearlist = new List<string>();

            var years = from m in _context.PlayerPitchings
                        orderby m.Year descending
                        select m.Year;

            yearlist.AddRange(years.Distinct());
            ViewBag.year = new SelectList(yearlist);

            //drop down list populating for sortType
            var sortlist = new List<string>();

            sortlist.Add("PlayerNumber");
            sortlist.Add("AtBat");
            sortlist.Add("EarnedRunAverage");
            sortlist.Add("InningsPitched");
            sortlist.Add("Runs");

            ViewBag.sortType = new SelectList(sortlist);

            //search operations
            var players = from m in _context.PlayerPitchings
                          select m;

            if (!String.IsNullOrEmpty(SearchString))
            {
                players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year));
                var count = players.Count();
                if (count != 0)
                {
                    //sort based on the sort parameter
                    switch (sortType)
                    {
                        case "PlayerNumber":
                            players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year)).OrderBy(s => s.PlayerNumber);
                            break;
                        case "AtBat":
                            players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.AtBat);
                            break;
                        case "EarnedRunAverage":
                            players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.EarnedRunAverage);
                            break;
                        case "InningsPitched":
                            players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.InningsPitched);
                            break;
                        default:
                            players = players.Where(s => (s.PlayerName.Contains(SearchString) && s.Year == year)).OrderByDescending(s => s.Runs);
                            break;
                    }

                    return View(players);
                }
                else
                {
                    ViewBag.noresult = "noresult";
                    return View(players);
                }
            }

            if (!String.IsNullOrEmpty(year) && !String.IsNullOrEmpty(sortType))
            {
                switch (sortType)
                {
                    case "PlayerNumber":
                        players = players.Where(s => (s.Year == year)).OrderBy(s => s.PlayerNumber);
                        break;
                    case "AtBat":
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.AtBat);
                        break;
                    case "EarnedRunAverage":
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.EarnedRunAverage);
                        break;
                    case "InningsPitched":
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.InningsPitched);
                        break;
                    default:
                        players = players.Where(s => (s.Year == year)).OrderByDescending(s => s.Runs);
                        break;
                }

                return View(players);
            }

            // default data to be displayed on the view initially.
            var pitchingss = from m in _context.PlayerPitchings
                             select m;

            pitchingss = pitchingss.Where(s => (s.Year == yearlist[0])).OrderBy(s => s.PlayerNumber);
            ViewBag.defualt = pitchingss;
            return View(pitchingss);
        }

        // GET: Players/pitching statistics
        /// <summary>
        /// Pitchings the statistics.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult PitchingStatistics(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var pstats = _context.PlayerPitchings.Single(m => m.PitchingID == id);
            ViewBag.stats = pstats;

            return View(pstats);
        }

        /// <summary>
        /// Fieldingses the specified sort order.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="SearchString">The search string.</param>
        /// <param name="year">The year.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult Fieldings(string sortOrder, string SearchString, string year)
        {
            //ViewBag.CurrentSort = sortOrder;
            ViewBag.playerNumberSort = String.IsNullOrEmpty(sortOrder) ? "player_desc" : "";
            ViewBag.nameSort = sortOrder == "Name" ? "name_desc" : "Name";

            //drop down list populating for year
            var yearlist = new List<string>();

            var years = from m in _context.PlayerFieldings
                        orderby m.Year descending
                        select m.Year;

            ViewBag.year = new SelectList(years.Distinct());

            //search operations
            var players = from m in _context.PlayerFieldings
                          select m;

            //var flag = false;
            if (!String.IsNullOrEmpty(SearchString))
            {
                players = players.Where(s => (s.PlayerName.Contains(SearchString)) && s.Year == year);
                return View(players);
            }

            if (!String.IsNullOrEmpty(year))
            {
                players = players.Where(s => (s.Year == year));
                return View(players);
            }

            //sort based on the sort parameter
            switch (sortOrder)
            {
                case "player_desc":
                    players = players.OrderByDescending(s => s.PlayerNumber);
                    break;
                case "Name":
                    players = players.OrderBy(s => s.PlayerName);
                    break;
                case "name_desc":
                    players = players.OrderByDescending(s => s.PlayerName);
                    break;
                default:
                    players = players.OrderBy(s => s.PlayerNumber);
                    break;
            }
            return View(players);
        }

        // GET: Players/batting statistics
        /// <summary>
        /// Battings the statistics.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult BattingStatistics(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var bstats = _context.PlayerBattings.Single(m => m.BattingID == id);
            ViewBag.stats = bstats;

            return View(bstats);
        }


        // GET: Players/Fielding statistics
        /// <summary>
        /// Fieldings the statistics.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IActionResult.</returns>
        public IActionResult FieldingStatistics(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var fieldingtats = _context.PlayerFieldings.Single(s => s.FieldingID == id);

            return View(fieldingtats);
        }

        //// GET: PlayerBattings
        //public IActionResult Index()
        //{
        //    return View(_context.PlayerBattings.ToList());
        //}

        //// GET: PlayerBattings/Details/5
        //public IActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    PlayerBatting playerBatting = _context.PlayerBattings.Single(m => m.BattingID == id);
        //    if (playerBatting == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    return View(playerBatting);
        ////}

        //// GET: PlayerBattings/Create
        //public IActionResult Create()
        //{
        //    return View();
        //}

        //// POST: PlayerBattings/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult Create(PlayerBatting playerBatting)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.PlayerBattings.Add(playerBatting);
        //        _context.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(playerBatting);
        //}

        //// GET: PlayerBattings/Edit/5
        //public IActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    PlayerBatting playerBatting = _context.PlayerBattings.Single(m => m.BattingID == id);
        //    if (playerBatting == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(playerBatting);
        //}

        //// POST: PlayerBattings/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult Edit(PlayerBatting playerBatting)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Update(playerBatting);
        //        _context.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(playerBatting);
        //}

        //// GET: PlayerBattings/Delete/5
        //[ActionName("Delete")]
        //public IActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    PlayerBatting playerBatting = _context.PlayerBattings.Single(m => m.BattingID == id);
        //    if (playerBatting == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    return View(playerBatting);
        //}

        //// POST: PlayerBattings/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public IActionResult DeleteConfirmed(int id)
        //{
        //    PlayerBatting playerBatting = _context.PlayerBattings.Single(m => m.BattingID == id);
        //    _context.PlayerBattings.Remove(playerBatting);
        //    _context.SaveChanges();
        //    return RedirectToAction("Index");
        //}
    }
}
