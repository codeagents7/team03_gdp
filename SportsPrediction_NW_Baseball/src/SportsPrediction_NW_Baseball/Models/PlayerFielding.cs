﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="PlayerFielding.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class PlayerFielding.
    /// </summary>
    public class PlayerFielding
    {
        /// <summary>
        /// Gets or sets the fielding identifier.
        /// </summary>
        /// <value>The fielding identifier.</value>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int FieldingID { get; set; }
        /// <summary>
        /// Gets or sets the player number.
        /// </summary>
        /// <value>The player number.</value>
        public int PlayerNumber { get; set; }
        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        /// <value>The name of the player.</value>
        public string PlayerName { get; set; }
        /// <summary>
        /// Gets or sets the catcher.
        /// </summary>
        /// <value>The catcher.</value>
        public int catcher { get; set; }
        /// <summary>
        /// Gets or sets the putout.
        /// </summary>
        /// <value>The putout.</value>
        public int Putout { get; set; }
        /// <summary>
        /// Gets or sets the assist.
        /// </summary>
        /// <value>The assist.</value>
        public int Assist { get; set; }
        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>The error.</value>
        public int Error { get; set; }
        /// <summary>
        /// Gets or sets the fielding percentage.
        /// </summary>
        /// <value>The fielding percentage.</value>
        public double FieldingPercentage { get; set; }
        /// <summary>
        /// Gets or sets the double play.
        /// </summary>
        /// <value>The double play.</value>
        public int DoublePlay { get; set; }
        /// <summary>
        /// Gets or sets the stolen balls attempted.
        /// </summary>
        /// <value>The stolen balls attempted.</value>
        public int StolenBallsAttempted { get; set; }
        /// <summary>
        /// Gets or sets the passed ball.
        /// </summary>
        /// <value>The passed ball.</value>
        public int PassedBall { get; set; }
        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>The year.</value>
        public string Year { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public string Type { get; set; }

        /// <summary>
        /// Reads all from CSV.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <returns>List&lt;PlayerFielding&gt;.</returns>
        public static List<PlayerFielding> ReadAllFromCSV(string filepath)
        {
            List<PlayerFielding> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => PlayerFielding.OneFromCsv(v))
                                         .ToList();
            return lst;
        }

        /// <summary>
        /// Called when [from CSV].
        /// </summary>
        /// <param name="csvLine">The CSV line.</param>
        /// <returns>PlayerFielding.</returns>
        public static PlayerFielding OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            PlayerFielding item = new PlayerFielding();

            int i = 0;

            item.FieldingID = Convert.ToInt32(values[i++]);
            item.PlayerNumber = Convert.ToInt32(values[i++]);
            item.PlayerName = Convert.ToString(values[i++]);
            item.catcher = Convert.ToInt32(values[i++]);
            item.Putout = Convert.ToInt32(values[i++]);
            item.Assist = Convert.ToInt32(values[i++]);
            item.Error = Convert.ToInt32(values[i++]);
            item.FieldingPercentage = Convert.ToDouble(values[i++]);
            item.DoublePlay = Convert.ToInt32(values[i++]);
            item.StolenBallsAttempted = Convert.ToInt32(values[i++]);
            item.PassedBall = Convert.ToInt32(values[i++]);
            item.Year = Convert.ToString(values[i++]);
            item.Type = Convert.ToString(values[i++]);
            
            return item;
        }

    }
}
