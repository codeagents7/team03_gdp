﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="GameBatting.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class GameBatting.
    /// </summary>
    public class GameBatting
    {
        /// <summary>
        /// Gets or sets the game batting identifier.
        /// </summary>
        /// <value>The game batting identifier.</value>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int GameBattingID { get; set; }
        /// <summary>
        /// Gets or sets the match date.
        /// </summary>
        /// <value>The match date.</value>
        public string Matchdate { get; set; }
        /// <summary>
        /// Gets or sets the opponent.
        /// </summary>
        /// <value>The opponent.</value>
        public string Opponent { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public string Status { get; set; }
        /// <summary>
        /// Gets or sets at bat.
        /// </summary>
        /// <value>At bat.</value>
        public int AtBat { get; set; }
        /// <summary>
        /// Gets or sets the runs.
        /// </summary>
        /// <value>The runs.</value>
        public int Runs { get; set; }
        /// <summary>
        /// Gets or sets the hits.
        /// </summary>
        /// <value>The hits.</value>
        public int Hits { get; set; }
        /// <summary>
        /// Gets or sets the runs batted in.
        /// </summary>
        /// <value>The runs batted in.</value>
        public int RunsBattedIn { get; set; }
        /// <summary>
        /// Gets or sets the double.
        /// </summary>
        /// <value>The double.</value>
        public int Double { get; set; }
        /// <summary>
        /// Gets or sets the triple.
        /// </summary>
        /// <value>The triple.</value>
        public int Triple { get; set; }
        /// <summary>
        /// Gets or sets the home runs.
        /// </summary>
        /// <value>The home runs.</value>
        public int HomeRuns { get; set; }
        /// <summary>
        /// Gets or sets the walk.
        /// </summary>
        /// <value>The walk.</value>
        public int Walk { get; set; }
        /// <summary>
        /// Gets or sets the intentional walk.
        /// </summary>
        /// <value>The intentional walk.</value>
        public int IntentionalWalk { get; set; }
        /// <summary>
        /// Gets or sets the stolen base.
        /// </summary>
        /// <value>The stolen base.</value>
        public int StolenBase { get; set; }
        /// <summary>
        /// Gets or sets the caught stealing.
        /// </summary>
        /// <value>The caught stealing.</value>
        public int CaughtStealing { get; set; }
        /// <summary>
        /// Gets or sets the hit by pitch.
        /// </summary>
        /// <value>The hit by pitch.</value>
        public int HitByPitch { get; set; }
        /// <summary>
        /// Gets or sets the sacrifice bunt.
        /// </summary>
        /// <value>The sacrifice bunt.</value>
        public int SacrificeBunt { get; set; }
        /// <summary>
        /// Gets or sets the sacrifice fly.
        /// </summary>
        /// <value>The sacrifice fly.</value>
        public int SacrificeFly { get; set; }
        /// <summary>
        /// Gets or sets the times groundedinto double plays.
        /// </summary>
        /// <value>The times groundedinto double plays.</value>
        public int TimesGroundedintoDoublePlays { get; set; }
        /// <summary>
        /// Gets or sets the strikeout.
        /// </summary>
        /// <value>The strikeout.</value>
        public int Strikeout { get; set; }
        /// <summary>
        /// Gets or sets the putout.
        /// </summary>
        /// <value>The putout.</value>
        public int Putout { get; set; }
        /// <summary>
        /// Gets or sets the assist.
        /// </summary>
        /// <value>The assist.</value>
        public int Assist { get; set; }
        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>The error.</value>
        public int Error { get; set; }
        /// <summary>
        /// Gets or sets the average.
        /// </summary>
        /// <value>The average.</value>
        public double Avg { get; set; }
        /// <summary>
        /// Gets or sets the season.
        /// </summary>
        /// <value>The season.</value>
        public string Season { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public string Type { get; set; }

        /// <summary>
        /// Reads all from CSV.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <returns>List&lt;GameBatting&gt;.</returns>
        public static List<GameBatting> ReadAllFromCSV(string filepath)
        {
            List<GameBatting> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => GameBatting.OneFromCsv(v))
                                         .ToList();
            return lst;
        }

        /// <summary>
        /// Called when [from CSV].
        /// </summary>
        /// <param name="csvLine">The CSV line.</param>
        /// <returns>GameBatting.</returns>
        public static GameBatting OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            GameBatting item = new GameBatting();
            int i = 0;

            item.GameBattingID = Convert.ToInt32(values[i++]);
            item.Matchdate = Convert.ToString(values[i++]);
            item.Opponent = Convert.ToString(values[i++]);
            item.Status = Convert.ToString(values[i++]);
            item.AtBat = Convert.ToInt32(values[i++]);
            item.Runs = Convert.ToInt32(values[i++]);
            item.Hits = Convert.ToInt32(values[i++]);
            item.RunsBattedIn = Convert.ToInt32(values[i++]);
            item.Double = Convert.ToInt32(values[i++]);
            item.Triple = Convert.ToInt32(values[i++]);
            item.HomeRuns = Convert.ToInt32(values[i++]);
            item.Walk = Convert.ToInt32(values[i++]);
            item.IntentionalWalk = Convert.ToInt32(values[i++]);
            item.StolenBase = Convert.ToInt32(values[i++]);
            item.CaughtStealing = Convert.ToInt32(values[i++]);
            item.HitByPitch = Convert.ToInt32(values[i++]);
            item.SacrificeBunt = Convert.ToInt32(values[i++]);
            item.SacrificeFly = Convert.ToInt32(values[i++]);
            item.TimesGroundedintoDoublePlays = Convert.ToInt32(values[i++]);
            item.Strikeout = Convert.ToInt32(values[i++]);
            item.Putout = Convert.ToInt32(values[i++]);
            item.Assist = Convert.ToInt32(values[i++]);
            item.Error = Convert.ToInt32(values[i++]);
            item.Avg = Convert.ToDouble(values[i++]);
            item.Season = Convert.ToString(values[i++]);
            item.Type = Convert.ToString(values[i++]);

            //Console.WriteLine(item);
            return item;
        }
    }
}
