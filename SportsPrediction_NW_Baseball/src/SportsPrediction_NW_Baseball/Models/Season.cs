﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="Season.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class Season.
    /// </summary>
    public class Season
    {
        /// <summary>
        /// Gets or sets the season identifier.
        /// </summary>
        /// <value>The season identifier.</value>
        [ScaffoldColumn(false)]
        [Key]
        public int SeasonId { get; set; }

        /// <summary>
        /// Gets or sets the name of the season.
        /// </summary>
        /// <value>The name of the season.</value>
        public string SeasonName { get; set; }
        /// <summary>
        /// Gets or sets the sonsers.
        /// </summary>
        /// <value>The sonsers.</value>
        public string Sonsers { get; set; }
        /// <summary>
        /// Gets or sets the time period.
        /// </summary>
        /// <value>The time period.</value>
        public string TimePeriod { get; set; }
        /// <summary>
        /// Gets or sets the player identifier.
        /// </summary>
        /// <value>The player identifier.</value>
        [ForeignKey("PlayerId")]
        public int PlayerId { get; set; }


    }
}
