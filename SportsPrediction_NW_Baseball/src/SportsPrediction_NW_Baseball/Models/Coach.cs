﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="Coach.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;


using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using System.ComponentModel.DataAnnotations.Schema;
namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class Coach.
    /// </summary>
    public class Coach
    {
        /// <summary>
        /// Gets or sets the coach identifier.
        /// </summary>
        /// <value>The coach identifier.</value>
        /// 

        [ScaffoldColumn(false)]
        [Key]
        public int CoachId { get; set; }
        /// <summary>
        /// Gets or sets the name of the coach.
        /// </summary>
        /// <value>The name of the coach.</value>
        public string CoachName { get; set; }
        /// <summary>
        /// Gets or sets the coach title.
        /// </summary>
        /// <value>The coach title.</value>
        [ForeignKey("SeasonId")]
        public string CoachTitle { get; set; }
    }
}
