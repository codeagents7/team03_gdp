﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="AppSeedData.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class AppSeedData.
    /// </summary>
    public class AppSeedData
    {
        /// <summary>
        /// Initializes the specified service provider.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="appPath">The application path.</param>
        /// <exception cref="System.Exception">DB is null</exception>
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<ApplicationDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            context.RemoveRange(context.Roaster);
            context.Roaster.AddRange(
           new Roaster()
           {
               FirstName = "Fort",
               LastName = "Garrett",
               Position = "INF, C",
               Highschool = "Anekny HS",
               Height = 6.1,
               Weight = 170,
               Hometown = "Ankeny - Iowa",
                AcademicClass = "Sophomore",
               Image = "~/lib/bootstrap/dist/images/Fort.jpg"
           },

            new Roaster()
            {
                FirstName = "Greenslade",
                LastName = " Kolby",
                Position = "INF",
                Highschool = "Anekny HS",
                Height = 6.1,
                Weight = 175,
                Hometown = "Norwalk - Iowa",
                            AcademicClass = "Sophomore",
                Image = "~/lib/bootstrap/dist/images/Greenslade.jpg"
            },
            new Roaster()
            {
                FirstName = "Gotta",
                LastName = " Nick",
                Position = "INF, C",
                Highschool = "Anekny HS",
                Height = 6.1,
                Weight = 3,
                Hometown = "Ankeny - Iowa",
                            AcademicClass = "Sophomore",
                Image = "~/lib/bootstrap/dist/images/Gotta.jpg"
            },
            new Roaster()
            {
                FirstName = "Handzlik",
                LastName = " Kevin",
                Position = "INF",
                Highschool = "Omaha Gross Catholic",
                Height = 6.1,
                Weight = 160,
                Hometown = "Ankeny - Iowa",
                            AcademicClass = "Freshman",
                Image = "~/lib/bootstrap/dist/images/Handzlik.jpg"
            }
            );
            context.SaveChanges();
            context.RemoveRange(context.Player);
            context.RemoveRange(context.PlayerFieldings);
            context.RemoveRange(context.PlayerBattings);
            context.RemoveRange(context.GamePitchings);
            context.RemoveRange(context.Player);
            context.RemoveRange(context.GameBattings);
            context.RemoveRange(context.PlayerPitchings);
            context.SaveChanges();

            SeedBattingsFromCsv(relPath, context);
            SeedBattingsFromCsv1(relPath, context);
            SeedPlayersFromCsv(relPath, context);
            SeedGamePitchingFromCsv(relPath, context);
            SeedGameBattingFromCsv(relPath, context);
            SeedPitchingFromCsv(relPath,context);
        }

        /// <summary>
        /// Seeds the battings from CSV.
        /// </summary>
        /// <param name="relPath">The relative path.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="System.Exception">Cannot find file " + source</exception>
        private static void SeedBattingsFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "PlayerFielding.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<PlayerFielding> lst = PlayerFielding.ReadAllFromCSV(source);
            context.PlayerFieldings.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        /// <summary>
        /// Seeds the battings from CSV1.
        /// </summary>
        /// <param name="relPath">The relative path.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="System.Exception">Cannot find file " + source</exception>
        private static void SeedBattingsFromCsv1(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "Playerbatting.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<PlayerBatting> lst = PlayerBatting.ReadAllFromCSV(source);
            context.PlayerBattings.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        /// <summary>
        /// Seeds the players from CSV.
        /// </summary>
        /// <param name="relpath">The relpath.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="System.Exception">Cannot find file " + source</exception>
        private static void SeedPlayersFromCsv(string relpath, ApplicationDbContext context)
        {
            string source = relpath + "Players.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Player> lst = Player.ReadAllFromCSV(source);
            context.Player.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        /// <summary>
        /// Seeds the game pitching from CSV.
        /// </summary>
        /// <param name="relPath">The relative path.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="System.Exception">Cannot find file " + source</exception>
        private static void SeedGamePitchingFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "GameByStatisticsPitching.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<GamePitching> lst = GamePitching.ReadAllFromCSV(source);
            context.GamePitchings.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        /// <summary>
        /// Seeds the game batting from CSV.
        /// </summary>
        /// <param name="relPath">The relative path.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="System.Exception">Cannot find file " + source</exception>
        private static void SeedGameBattingFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "GameByStatisticsBatting.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<GameBatting> lst = GameBatting.ReadAllFromCSV(source);
            context.GameBattings.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        /// <summary>
        /// Seeds the pitching from CSV.
        /// </summary>
        /// <param name="relPath">The relative path.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="System.Exception">Cannot find file " + source</exception>
        private static void SeedPitchingFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "PlayerPitching.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }

            List<PlayerPitching> lst = PlayerPitching.ReadAllFromCSV(source);
            context.PlayerPitchings.AddRange(lst.ToArray());
            context.SaveChanges();
        }

    }
}


