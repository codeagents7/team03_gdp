﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="PlayerPitching.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class PlayerPitching.
    /// </summary>
    public class PlayerPitching
    {
        /// <summary>
        /// Gets or sets the pitching identifier.
        /// </summary>
        /// <value>The pitching identifier.</value>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int PitchingID { get; set; }
        /// <summary>
        /// Gets or sets the player number.
        /// </summary>
        /// <value>The player number.</value>
        public int PlayerNumber { get; set; }
        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        /// <value>The name of the player.</value>
        public string PlayerName { get; set; }
        /// <summary>
        /// Gets or sets the earned run average.
        /// </summary>
        /// <value>The earned run average.</value>
        public double EarnedRunAverage { get; set; }
        /// <summary>
        /// Gets or sets the wins losses.
        /// </summary>
        /// <value>The wins losses.</value>
        public string winsLosses { get; set; }
        /// <summary>
        /// Gets or sets the appearance games started.
        /// </summary>
        /// <value>The appearance games started.</value>
        public string AppearanceGamesStarted { get; set; }
        /// <summary>
        /// Gets or sets the complete game.
        /// </summary>
        /// <value>The complete game.</value>
        public int CompleteGame { get; set; }
        /// <summary>
        /// Gets or sets the shutout.
        /// </summary>
        /// <value>The shutout.</value>
        public string Shutout { get; set; }
        /// <summary>
        /// Gets or sets the save.
        /// </summary>
        /// <value>The save.</value>
        public int Save { get; set; }
        /// <summary>
        /// Gets or sets the innings pitched.
        /// </summary>
        /// <value>The innings pitched.</value>
        public double InningsPitched { get; set; }
        /// <summary>
        /// Gets or sets the hold.
        /// </summary>
        /// <value>The hold.</value>
        public int Hold { get; set; }
        /// <summary>
        /// Gets or sets the runs.
        /// </summary>
        /// <value>The runs.</value>
        public int Runs { get; set; }
        /// <summary>
        /// Gets or sets the earned runs.
        /// </summary>
        /// <value>The earned runs.</value>
        public int EarnedRuns { get; set; }
        /// <summary>
        /// Gets or sets the walks.
        /// </summary>
        /// <value>The walks.</value>
        public int Walks { get; set; }
        /// <summary>
        /// Gets or sets the strike out.
        /// </summary>
        /// <value>The strike out.</value>
        public int StrikeOut { get; set; }
        /// <summary>
        /// Gets or sets the double.
        /// </summary>
        /// <value>The double.</value>
        public int Double { get; set; }
        /// <summary>
        /// Gets or sets the triple.
        /// </summary>
        /// <value>The triple.</value>
        public int Triple { get; set; }
        /// <summary>
        /// Gets or sets the home run.
        /// </summary>
        /// <value>The home run.</value>
        public int HomeRun { get; set; }
        /// <summary>
        /// Gets or sets at bat.
        /// </summary>
        /// <value>At bat.</value>
        public int AtBat { get; set; }
        /// <summary>
        /// Gets or sets the b average.
        /// </summary>
        /// <value>The b average.</value>
        public double BAverage { get; set; }
        /// <summary>
        /// Gets or sets the wild pitch.
        /// </summary>
        /// <value>The wild pitch.</value>
        public int WildPitch { get; set; }
        /// <summary>
        /// Gets or sets the hit by pitch.
        /// </summary>
        /// <value>The hit by pitch.</value>
        public int HitByPitch { get; set; }
        /// <summary>
        /// Gets or sets the balk.
        /// </summary>
        /// <value>The balk.</value>
        public int Balk { get; set; }
        /// <summary>
        /// Gets or sets the sacrifice fly.
        /// </summary>
        /// <value>The sacrifice fly.</value>
        public int SacrificeFly { get; set; }
        /// <summary>
        /// Gets or sets the sacrifice hit.
        /// </summary>
        /// <value>The sacrifice hit.</value>
        public int SacrificeHit { get; set; }
        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>The year.</value>
        public string Year { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public string Type { get; set; }


        /// <summary>
        /// Reads all from CSV.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <returns>List&lt;PlayerPitching&gt;.</returns>
        public static List<PlayerPitching> ReadAllFromCSV(string filepath)
        {
            List<PlayerPitching> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => PlayerPitching.OneFromCsv(v))
                                         .ToList();
            return lst;
        }

        /// <summary>
        /// Called when [from CSV].
        /// </summary>
        /// <param name="csvLine">The CSV line.</param>
        /// <returns>PlayerPitching.</returns>
        public static PlayerPitching OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            PlayerPitching item = new PlayerPitching();

            int i = 0;
            item.PitchingID = Convert.ToInt32(values[i++]);
            item.PlayerNumber = Convert.ToInt32(values[i++]);
            item.PlayerName = Convert.ToString(values[i++]);
            item.EarnedRunAverage = Convert.ToDouble(values[i++]);
            item.winsLosses = Convert.ToString(values[i++]);
            item.AppearanceGamesStarted = Convert.ToString(values[i++]);
            item.CompleteGame = Convert.ToInt32(values[i++]);
            item.Shutout = Convert.ToString(values[i++]);
            item.Save = Convert.ToInt32(values[i++]);
            item.InningsPitched = Convert.ToDouble(values[i++]);
            item.Hold = Convert.ToInt32(values[i++]);
            item.Runs = Convert.ToInt32(values[i++]);
            item.EarnedRuns = Convert.ToInt32(values[i++]);
            item.Walks = Convert.ToInt32(values[i++]);
            item.StrikeOut = Convert.ToInt32(values[i++]);
            item.Double = Convert.ToInt32(values[i++]);
            item.Triple = Convert.ToInt32(values[i++]);
            item.HomeRun = Convert.ToInt32(values[i++]);
            item.AtBat = Convert.ToInt32(values[i++]);
            item.BAverage = Convert.ToDouble(values[i++]);
            item.WildPitch = Convert.ToInt32(values[i++]);
            item.HitByPitch = Convert.ToInt32(values[i++]);
            item.Balk = Convert.ToInt32(values[i++]);
            item.SacrificeFly = Convert.ToInt32(values[i++]);
            item.SacrificeHit = Convert.ToInt32(values[i++]);
            item.Year = Convert.ToString(values[i++]);
            item.Type = Convert.ToString(values[i++]);

            return item;
        }
    }
}
