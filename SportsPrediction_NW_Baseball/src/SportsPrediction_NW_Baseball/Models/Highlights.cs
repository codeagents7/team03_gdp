﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="Highlights.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class Highlights.
    /// </summary>
    public class Highlights
    {
        /// <summary>
        /// Gets or sets the highlight identifier.
        /// </summary>
        /// <value>The highlight identifier.</value>
        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HighlightId { get; set; }
        /// <summary>
        /// Gets or sets the name of the highlight.
        /// </summary>
        /// <value>The name of the highlight.</value>
        [Required]
        public string HighlightName { get; set; }
        /// <summary>
        /// Gets or sets the season identifier.
        /// </summary>
        /// <value>The season identifier.</value>
        public string SeasonId { get; set; }
    }
}
