﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="Games.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SportsPrediction_NW_Baseball.Models {

    /// <summary>
    /// Class Games.
    /// </summary>
    public class Games
{
        /// <summary>
        /// Gets or sets the games identifier.
        /// </summary>
        /// <value>The games identifier.</value>
        [ScaffoldColumn(false)]
    [Key]
    public int GamesId { get; set; }
        /// <summary>
        /// Gets or sets the match date.
        /// </summary>
        /// <value>The match date.</value>
        public DateTime MatchDate { get; set; }
        /// <summary>
        /// Gets or sets the name of the opponent.
        /// </summary>
        /// <value>The name of the opponent.</value>
        public string OpponentName { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public string Status { get; set; }
        /// <summary>
        /// Gets or sets at bat.
        /// </summary>
        /// <value>At bat.</value>
        public int AtBat { get; set; }
        /// <summary>
        /// Gets or sets the runs.
        /// </summary>
        /// <value>The runs.</value>
        public int Runs { get; set; }
        /// <summary>
        /// Gets or sets the hits.
        /// </summary>
        /// <value>The hits.</value>
        public int Hits { get; set; }
        /// <summary>
        /// Gets or sets the average.
        /// </summary>
        /// <value>The average.</value>
        public int Average { get; set; }
        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>The year.</value>
        public int year { get; set; }

}
}
