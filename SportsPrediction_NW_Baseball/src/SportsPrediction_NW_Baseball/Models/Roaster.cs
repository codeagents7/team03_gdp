﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="Roaster.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class Roaster.
    /// </summary>
    public class Roaster
    {


        /// <summary>
        /// Gets or sets the roaster identifier.
        /// </summary>
        /// <value>The roaster identifier.</value>
        [Key]
        public int RoasterID { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>The position.</value>
        public string Position { get; set; }

        /// <summary>
        /// Gets or sets the highschool.
        /// </summary>
        /// <value>The highschool.</value>
        public String Highschool { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>The height.</value>
        public double Height { get; set; }

        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        /// <value>The weight.</value>
        public int Weight { get; set; }

        /// <summary>
        /// Gets or sets the hometown.
        /// </summary>
        /// <value>The hometown.</value>
        public string Hometown { get; set; }

        /// <summary>
        /// Gets or sets the academic class.
        /// </summary>
        /// <value>The academic class.</value>
        public string AcademicClass { get; set; }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>The image.</value>
        public String Image { get; set; }
    }
}
