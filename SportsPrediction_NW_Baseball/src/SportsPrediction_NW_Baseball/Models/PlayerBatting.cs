﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="PlayerBatting.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;


namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class PlayerBatting.
    /// </summary>
    public class PlayerBatting
    {
        /// <summary>
        /// Gets or sets the batting identifier.
        /// </summary>
        /// <value>The batting identifier.</value>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int BattingID { get; set; }
        /// <summary>
        /// Gets or sets the player number.
        /// </summary>
        /// <value>The player number.</value>
        public int PlayerNumber { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the average.
        /// </summary>
        /// <value>The average.</value>
        public double Average { get; set; }
        /// <summary>
        /// Gets or sets the games played.
        /// </summary>
        /// <value>The games played.</value>
        public int GamesPlayed { get; set; }
        /// <summary>
        /// Gets or sets the games started.
        /// </summary>
        /// <value>The games started.</value>
        public int GamesStarted { get; set; }
        /// <summary>
        /// Gets or sets at bat.
        /// </summary>
        /// <value>At bat.</value>
        public int AtBat { get; set; }
        /// <summary>
        /// Gets or sets the runs.
        /// </summary>
        /// <value>The runs.</value>
        public int Runs { get; set; }
        /// <summary>
        /// Gets or sets the hits.
        /// </summary>
        /// <value>The hits.</value>
        public int Hits { get; set; }
        /// <summary>
        /// Gets or sets the double.
        /// </summary>
        /// <value>The double.</value>
        public int Double { get; set; }
        /// <summary>
        /// Gets or sets the triple.
        /// </summary>
        /// <value>The triple.</value>
        public int Triple { get; set; }
        /// <summary>
        /// Gets or sets the home run.
        /// </summary>
        /// <value>The home run.</value>
        public int HomeRun { get; set; }
        /// <summary>
        /// Gets or sets the runs batted in.
        /// </summary>
        /// <value>The runs batted in.</value>
        public int RunsBattedIn { get; set; }
        /// <summary>
        /// Gets or sets the total bases.
        /// </summary>
        /// <value>The total bases.</value>
        public int TotalBases { get; set; }
        /// <summary>
        /// Gets or sets the stolen base.
        /// </summary>
        /// <value>The stolen base.</value>
        public int StolenBase { get; set; }
        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>The year.</value>
        public string Year { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public string Type { get; set; }


        /// <summary>
        /// Reads all from CSV.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <returns>List&lt;PlayerBatting&gt;.</returns>
        public static List<PlayerBatting> ReadAllFromCSV(string filepath)
        {
            List<PlayerBatting> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => PlayerBatting.OneFromCsv(v))
                                         .ToList();
            return lst;
        }

        /// <summary>
        /// Called when [from CSV].
        /// </summary>
        /// <param name="csvLine">The CSV line.</param>
        /// <returns>PlayerBatting.</returns>
        public static PlayerBatting OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            PlayerBatting item = new PlayerBatting();

            int i = 0;
            item.BattingID = Convert.ToInt32(values[i++]);
            item.PlayerNumber = Convert.ToInt32(values[i++]);
            item.Name = Convert.ToString(values[i++]);
            item.Average = Convert.ToDouble(values[i++]);
            item.GamesPlayed = Convert.ToInt32(values[i++]);
            item.GamesStarted = Convert.ToInt32(values[i++]);
            item.AtBat = Convert.ToInt32(values[i++]);
            item.Runs = Convert.ToInt32(values[i++]);
            item.Hits = Convert.ToInt32(values[i++]);
            item.Double = Convert.ToInt32(values[i++]);
            item.Triple = Convert.ToInt32(values[i++]);
            item.HomeRun = Convert.ToInt32(values[i++]);
            item.RunsBattedIn = Convert.ToInt32(values[i++]);
            item.TotalBases = Convert.ToInt32(values[i++]);
            item.StolenBase = Convert.ToInt32(values[i++]);
            item.Year = Convert.ToString(values[i++]);
            item.Type = Convert.ToString(values[i++]);

            return item;
        }
    }
}
