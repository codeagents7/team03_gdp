﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="Player.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class Player.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets the player number.
        /// </summary>
        /// <value>The player number.</value>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        
        [Key]
        public int PlayerNumber { get; set; }
        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        /// <value>The name of the player.</value>
        public string PlayerName { get; set; }
        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>The position.</value>
        public string Position { get; set; }
        /// <summary>
        /// Gets or sets the bats throws.
        /// </summary>
        /// <value>The bats throws.</value>
        public string Bats_Throws { get; set; }
        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>The height.</value>
        public string height { get; set; }
        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        /// <value>The weight.</value>
        public string weight { get; set; }
        /// <summary>
        /// Gets or sets the hometown highschool.
        /// </summary>
        /// <value>The hometown highschool.</value>
        public string Hometown_Highschool { get; set; }


        /// <summary>
        /// Reads all from CSV.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <returns>List&lt;Player&gt;.</returns>
        public static List<Player> ReadAllFromCSV(string filepath)
        {
            List<Player> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => Player.OneFromCsv(v))
                                         .ToList();
            return lst;
        }
        /// <summary>
        /// Called when [from CSV].
        /// </summary>
        /// <param name="csvLine">The CSV line.</param>
        /// <returns>Player.</returns>
        public static Player OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Player item = new Player();
            int i = 0;
            item.PlayerNumber = Convert.ToInt32(values[i++]);
            item.PlayerName = Convert.ToString(values[i++]);
            item.Position = Convert.ToString(values[i++]);
            item.Bats_Throws = Convert.ToString(values[i++]);
            item.height = Convert.ToString(values[i++]);
            item.weight = Convert.ToString(values[i++]);
            item.Hometown_Highschool = Convert.ToString(values[i++]);
           
            //Console.WriteLine(item);
            return item;
        }


    }
}
