﻿// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="GamePitching.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class GamePitching.
    /// </summary>
    public class GamePitching
{
        /// <summary>
        /// Gets or sets the game pitching identifier.
        /// </summary>
        /// <value>The game pitching identifier.</value>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
    [Key]
    public int GamePitchingID { get; set; }
        /// <summary>
        /// Gets or sets the matchdate.
        /// </summary>
        /// <value>The matchdate.</value>
        public string Matchdate { get; set; }
        /// <summary>
        /// Gets or sets the opponent.
        /// </summary>
        /// <value>The opponent.</value>
        public string Opponent  { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public string Status { get; set; }
        /// <summary>
        /// Gets or sets the innings pitched.
        /// </summary>
        /// <value>The innings pitched.</value>
        public double InningsPitched { get; set; }
        /// <summary>
        /// Gets or sets the hits.
        /// </summary>
        /// <value>The hits.</value>
        public int Hits { get; set; }
        /// <summary>
        /// Gets or sets the runs.
        /// </summary>
        /// <value>The runs.</value>
        public int Runs { get; set; }
        /// <summary>
        /// Gets or sets the earned runs.
        /// </summary>
        /// <value>The earned runs.</value>
        public int EarnedRuns { get; set; }
        /// <summary>
        /// Gets or sets the walk.
        /// </summary>
        /// <value>The walk.</value>
        public int Walk { get; set; }
        /// <summary>
        /// Gets or sets the strikeouts.
        /// </summary>
        /// <value>The strikeouts.</value>
        public int Strikeouts { get; set; }
        /// <summary>
        /// Gets or sets the double.
        /// </summary>
        /// <value>The double.</value>
        public int Double { get; set; }
        /// <summary>
        /// Gets or sets the triple.
        /// </summary>
        /// <value>The triple.</value>
        public int Triple { get; set; }
        /// <summary>
        /// Gets or sets the home runs.
        /// </summary>
        /// <value>The home runs.</value>
        public int HomeRuns { get; set; }
        /// <summary>
        /// Gets or sets the wild pitches.
        /// </summary>
        /// <value>The wild pitches.</value>
        public int WildPitches { get; set; }
        /// <summary>
        /// Gets or sets the balks.
        /// </summary>
        /// <value>The balks.</value>
        public int Balks { get; set; }
        /// <summary>
        /// Gets or sets the hit by pitch.
        /// </summary>
        /// <value>The hit by pitch.</value>
        public int HitByPitch { get; set; }
        /// <summary>
        /// Gets or sets the intentional walk.
        /// </summary>
        /// <value>The intentional walk.</value>
        public int IntentionalWalk { get; set; }
        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        /// <value>The score.</value>
        public string Score { get; set; }
        /// <summary>
        /// Gets or sets the wins.
        /// </summary>
        /// <value>The wins.</value>
        public int Wins { get; set; }
        /// <summary>
        /// Gets or sets the losses.
        /// </summary>
        /// <value>The losses.</value>
        public int Losses { get; set; }
        /// <summary>
        /// Gets or sets the saves.
        /// </summary>
        /// <value>The saves.</value>
        public int Saves { get; set; }
        /// <summary>
        /// Gets or sets the earned run average.
        /// </summary>
        /// <value>The earned run average.</value>
        public double EarnedRunAverage { get; set; }
        /// <summary>
        /// Gets or sets the season.
        /// </summary>
        /// <value>The season.</value>
        public string Season { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public string Type { get; set; }

        /// <summary>
        /// Reads all from CSV.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <returns>List&lt;GamePitching&gt;.</returns>
        public static List<GamePitching> ReadAllFromCSV(string filepath)
        {
            List<GamePitching> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => GamePitching.OneFromCsv(v))
                                         .ToList();
            return lst;
        }

        /// <summary>
        /// Called when [from CSV].
        /// </summary>
        /// <param name="csvLine">The CSV line.</param>
        /// <returns>GamePitching.</returns>
        public static GamePitching OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            GamePitching item = new GamePitching();
            int i = 0;
            item.GamePitchingID = Convert.ToInt32(values[i++]);
            item.Matchdate = Convert.ToString(values[i++]);
            item.Opponent = Convert.ToString(values[i++]);
            item.Status = Convert.ToString(values[i++]);
            item.InningsPitched = Convert.ToDouble(values[i++]);
            item.Hits = Convert.ToInt32(values[i++]);
            item.Runs = Convert.ToInt32(values[i++]);
            item.EarnedRuns = Convert.ToInt32(values[i++]);
            item.Walk = Convert.ToInt32(values[i++]);
            item.Strikeouts = Convert.ToInt32(values[i++]);
            item.Double = Convert.ToInt32(values[i++]);
            item.Triple = Convert.ToInt32(values[i++]);
            item.HomeRuns = Convert.ToInt32(values[i++]);
            item.WildPitches = Convert.ToInt32(values[i++]);
            item.Balks = Convert.ToInt32(values[i++]);
            item.HitByPitch = Convert.ToInt32(values[i++]);
            item.IntentionalWalk = Convert.ToInt32(values[i++]);
            item.Score = Convert.ToString(values[i++]);
            item.Wins = Convert.ToInt32(values[i++]);
            item.Losses = Convert.ToInt32(values[i++]);
            item.Saves = Convert.ToInt32(values[i++]);
            item.EarnedRunAverage = Convert.ToDouble(values[i++]);
            item.Season = Convert.ToString(values[i++]);
            item.Type = Convert.ToString(values[i++]);

            //Console.WriteLine(item);
            return item;
        }
}

}