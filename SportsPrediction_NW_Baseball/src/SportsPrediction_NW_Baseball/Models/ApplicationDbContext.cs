﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using SportsPrediction_NW_Baseball.Models;

namespace SportsPrediction_NW_Baseball.Models
{
    /// <summary>
    /// Class ApplicationDbContext.
    /// </summary>
    /// <seealso cref="Microsoft.AspNet.Identity.EntityFramework.IdentityDbContext{SportsPrediction_NW_Baseball.Models.ApplicationUser}" />
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        /// <summary>
        /// Gets or sets the player fieldings.
        /// </summary>
        /// <value>The player fieldings.</value>
        public DbSet<PlayerFielding> PlayerFieldings { get; set; }
        /// <summary>
        /// Gets or sets the player battings.
        /// </summary>
        /// <value>The player battings.</value>
        public DbSet<PlayerBatting> PlayerBattings { get; set; }
        /// <summary>
        /// Gets or sets the player pitchings.
        /// </summary>
        /// <value>The player pitchings.</value>
        public DbSet<PlayerPitching> PlayerPitchings { get; set; }
        public DbSet<Player> Player { get; set; }
        public DbSet<GamePitching> GamePitchings { get; set; }
        /// <summary>
        /// Gets or sets the game battings.
        /// </summary>
        /// <value>The game battings.</value>
        public DbSet<GameBatting> GameBattings { get; set; }
        /// <summary>
        /// Gets or sets the games.
        /// </summary>
        /// <value>The games.</value>
        public DbSet<Games> Games { get; set; }
        /// <summary>
        /// Gets or sets the roaster.
        /// </summary>
        /// <value>The roaster.</value>
        public DbSet<Roaster> Roaster { get; set; }
    }
}




