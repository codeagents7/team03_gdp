// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="ApplicationDbContextModelSnapshot.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using SportsPrediction_NW_Baseball.Models;

namespace SportsPrediction_NW_Baseball.Migrations
{
    /// <summary>
    /// Class ApplicationDbContextModelSnapshot.
    /// </summary>
    /// <seealso cref="Microsoft.Data.Entity.Infrastructure.ModelSnapshot" />
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        /// <summary>
        /// Builds the model.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasAnnotation("Relational:Name", "RoleNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasAnnotation("Relational:TableName", "AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasAnnotation("Relational:TableName", "AspNetUserRoles");
                });

            modelBuilder.Entity("SportsPrediction_NW_Baseball.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasAnnotation("Relational:Name", "EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .HasAnnotation("Relational:Name", "UserNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("SportsPrediction_NW_Baseball.Models.GameBatting", b =>
                {
                    b.Property<int>("GameBattingID");

                    b.Property<int>("Assist");

                    b.Property<int>("AtBat");

                    b.Property<double>("Avg");

                    b.Property<int>("CaughtStealing");

                    b.Property<int>("Double");

                    b.Property<int>("Error");

                    b.Property<int>("HitByPitch");

                    b.Property<int>("Hits");

                    b.Property<int>("HomeRuns");

                    b.Property<int>("IntentionalWalk");

                    b.Property<string>("Matchdate");

                    b.Property<string>("Opponent");

                    b.Property<int>("Putout");

                    b.Property<int>("Runs");

                    b.Property<int>("RunsBattedIn");

                    b.Property<int>("SacrificeBunt");

                    b.Property<int>("SacrificeFly");

                    b.Property<string>("Season");

                    b.Property<string>("Status");

                    b.Property<int>("StolenBase");

                    b.Property<int>("Strikeout");

                    b.Property<int>("TimesGroundedintoDoublePlays");

                    b.Property<int>("Triple");

                    b.Property<string>("Type");

                    b.Property<int>("Walk");

                    b.HasKey("GameBattingID");
                });

            modelBuilder.Entity("SportsPrediction_NW_Baseball.Models.GamePitching", b =>
                {
                    b.Property<int>("GamePitchingID");

                    b.Property<int>("Balks");

                    b.Property<int>("Double");

                    b.Property<double>("EarnedRunAverage");

                    b.Property<int>("EarnedRuns");

                    b.Property<int>("HitByPitch");

                    b.Property<int>("Hits");

                    b.Property<int>("HomeRuns");

                    b.Property<double>("InningsPitched");

                    b.Property<int>("IntentionalWalk");

                    b.Property<int>("Losses");

                    b.Property<string>("Matchdate");

                    b.Property<string>("Opponent");

                    b.Property<int>("Runs");

                    b.Property<int>("Saves");

                    b.Property<string>("Score");

                    b.Property<string>("Season");

                    b.Property<string>("Status");

                    b.Property<int>("Strikeouts");

                    b.Property<int>("Triple");

                    b.Property<string>("Type");

                    b.Property<int>("Walk");

                    b.Property<int>("WildPitches");

                    b.Property<int>("Wins");

                    b.HasKey("GamePitchingID");
                });

            modelBuilder.Entity("SportsPrediction_NW_Baseball.Models.Games", b =>
                {
                    b.Property<int>("GamesId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AtBat");

                    b.Property<int>("Average");

                    b.Property<int>("Hits");

                    b.Property<DateTime>("MatchDate");

                    b.Property<string>("OpponentName");

                    b.Property<int>("Runs");

                    b.Property<string>("Status");

                    b.Property<int>("year");

                    b.HasKey("GamesId");
                });

            modelBuilder.Entity("SportsPrediction_NW_Baseball.Models.Player", b =>
                {
                    b.Property<int>("PlayerNumber");

                    b.Property<string>("Bats_Throws");

                    b.Property<string>("Hometown_Highschool");

                    b.Property<string>("PlayerName");

                    b.Property<string>("Position");

                    b.Property<string>("height");

                    b.Property<string>("weight");

                    b.HasKey("PlayerNumber");
                });

            modelBuilder.Entity("SportsPrediction_NW_Baseball.Models.PlayerBatting", b =>
                {
                    b.Property<int>("BattingID");

                    b.Property<int>("AtBat");

                    b.Property<double>("Average");

                    b.Property<int>("Double");

                    b.Property<int>("GamesPlayed");

                    b.Property<int>("GamesStarted");

                    b.Property<int>("Hits");

                    b.Property<int>("HomeRun");

                    b.Property<string>("Name");

                    b.Property<int>("PlayerNumber");

                    b.Property<int>("Runs");

                    b.Property<int>("RunsBattedIn");

                    b.Property<int>("StolenBase");

                    b.Property<int>("TotalBases");

                    b.Property<int>("Triple");

                    b.Property<string>("Type");

                    b.Property<string>("Year");

                    b.HasKey("BattingID");
                });

            modelBuilder.Entity("SportsPrediction_NW_Baseball.Models.PlayerFielding", b =>
                {
                    b.Property<int>("FieldingID");

                    b.Property<int>("Assist");

                    b.Property<int>("DoublePlay");

                    b.Property<int>("Error");

                    b.Property<double>("FieldingPercentage");

                    b.Property<int>("PassedBall");

                    b.Property<string>("PlayerName");

                    b.Property<int>("PlayerNumber");

                    b.Property<int>("Putout");

                    b.Property<int>("StolenBallsAttempted");

                    b.Property<string>("Type");

                    b.Property<string>("Year");

                    b.Property<int>("catcher");

                    b.HasKey("FieldingID");
                });

            modelBuilder.Entity("SportsPrediction_NW_Baseball.Models.PlayerPitching", b =>
                {
                    b.Property<int>("PitchingID");

                    b.Property<string>("AppearanceGamesStarted");

                    b.Property<int>("AtBat");

                    b.Property<double>("BAverage");

                    b.Property<int>("Balk");

                    b.Property<int>("CompleteGame");

                    b.Property<int>("Double");

                    b.Property<double>("EarnedRunAverage");

                    b.Property<int>("EarnedRuns");

                    b.Property<int>("HitByPitch");

                    b.Property<int>("Hold");

                    b.Property<int>("HomeRun");

                    b.Property<double>("InningsPitched");

                    b.Property<string>("PlayerName");

                    b.Property<int>("PlayerNumber");

                    b.Property<int>("Runs");

                    b.Property<int>("SacrificeFly");

                    b.Property<int>("SacrificeHit");

                    b.Property<int>("Save");

                    b.Property<string>("Shutout");

                    b.Property<int>("StrikeOut");

                    b.Property<int>("Triple");

                    b.Property<string>("Type");

                    b.Property<int>("Walks");

                    b.Property<int>("WildPitch");

                    b.Property<string>("Year");

                    b.Property<string>("winsLosses");

                    b.HasKey("PitchingID");
                });

            modelBuilder.Entity("SportsPrediction_NW_Baseball.Models.Roaster", b =>
                {
                    b.Property<int>("RoasterID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AcademicClass");

                    b.Property<string>("FirstName");

                    b.Property<double>("Height");

                    b.Property<string>("Highschool");

                    b.Property<string>("Hometown");

                    b.Property<string>("Image");

                    b.Property<string>("LastName");

                    b.Property<string>("Position");

                    b.Property<int>("Weight");

                    b.HasKey("RoasterID");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("SportsPrediction_NW_Baseball.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("SportsPrediction_NW_Baseball.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("SportsPrediction_NW_Baseball.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });
        }
    }
}
