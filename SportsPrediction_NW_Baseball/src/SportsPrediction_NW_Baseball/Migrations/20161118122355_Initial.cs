// ***********************************************************************
// Assembly         : 
// Author           : S525033
// Created          : 11-18-2016
//
// Last Modified By : S525033
// Last Modified On : 11-18-2016
// ***********************************************************************
// <copyright file="20161118122355_Initial.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace SportsPrediction_NW_Baseball.Migrations
{
    /// <summary>
    /// Class Initial.
    /// </summary>
    /// <seealso cref="Microsoft.Data.Entity.Migrations.Migration" />
    public partial class Initial : Migration
    {
        /// <summary>
        /// Ups the specified migration builder.
        /// </summary>
        /// <param name="migrationBuilder">The migration builder.</param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NormalizedName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityRole", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(nullable: true),
                    NormalizedUserName = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUser", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "GameBatting",
                columns: table => new
                {
                    GameBattingID = table.Column<int>(nullable: false),
                    Assist = table.Column<int>(nullable: false),
                    AtBat = table.Column<int>(nullable: false),
                    Avg = table.Column<double>(nullable: false),
                    CaughtStealing = table.Column<int>(nullable: false),
                    Double = table.Column<int>(nullable: false),
                    Error = table.Column<int>(nullable: false),
                    HitByPitch = table.Column<int>(nullable: false),
                    Hits = table.Column<int>(nullable: false),
                    HomeRuns = table.Column<int>(nullable: false),
                    IntentionalWalk = table.Column<int>(nullable: false),
                    Matchdate = table.Column<string>(nullable: true),
                    Opponent = table.Column<string>(nullable: true),
                    Putout = table.Column<int>(nullable: false),
                    Runs = table.Column<int>(nullable: false),
                    RunsBattedIn = table.Column<int>(nullable: false),
                    SacrificeBunt = table.Column<int>(nullable: false),
                    SacrificeFly = table.Column<int>(nullable: false),
                    Season = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    StolenBase = table.Column<int>(nullable: false),
                    Strikeout = table.Column<int>(nullable: false),
                    TimesGroundedintoDoublePlays = table.Column<int>(nullable: false),
                    Triple = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Walk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameBatting", x => x.GameBattingID);
                });
            migrationBuilder.CreateTable(
                name: "GamePitching",
                columns: table => new
                {
                    GamePitchingID = table.Column<int>(nullable: false),
                    Balks = table.Column<int>(nullable: false),
                    Double = table.Column<int>(nullable: false),
                    EarnedRunAverage = table.Column<double>(nullable: false),
                    EarnedRuns = table.Column<int>(nullable: false),
                    HitByPitch = table.Column<int>(nullable: false),
                    Hits = table.Column<int>(nullable: false),
                    HomeRuns = table.Column<int>(nullable: false),
                    InningsPitched = table.Column<double>(nullable: false),
                    IntentionalWalk = table.Column<int>(nullable: false),
                    Losses = table.Column<int>(nullable: false),
                    Matchdate = table.Column<string>(nullable: true),
                    Opponent = table.Column<string>(nullable: true),
                    Runs = table.Column<int>(nullable: false),
                    Saves = table.Column<int>(nullable: false),
                    Score = table.Column<string>(nullable: true),
                    Season = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Strikeouts = table.Column<int>(nullable: false),
                    Triple = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Walk = table.Column<int>(nullable: false),
                    WildPitches = table.Column<int>(nullable: false),
                    Wins = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GamePitching", x => x.GamePitchingID);
                });
            migrationBuilder.CreateTable(
                name: "Games",
                columns: table => new
                {
                    GamesId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AtBat = table.Column<int>(nullable: false),
                    Average = table.Column<int>(nullable: false),
                    Hits = table.Column<int>(nullable: false),
                    MatchDate = table.Column<DateTime>(nullable: false),
                    OpponentName = table.Column<string>(nullable: true),
                    Runs = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.GamesId);
                });
            migrationBuilder.CreateTable(
                name: "Player",
                columns: table => new
                {
                    PlayerNumber = table.Column<int>(nullable: false),
                    Bats_Throws = table.Column<string>(nullable: true),
                    Hometown_Highschool = table.Column<string>(nullable: true),
                    PlayerName = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    height = table.Column<string>(nullable: true),
                    weight = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Player", x => x.PlayerNumber);
                });
            migrationBuilder.CreateTable(
                name: "PlayerBatting",
                columns: table => new
                {
                    BattingID = table.Column<int>(nullable: false),
                    AtBat = table.Column<int>(nullable: false),
                    Average = table.Column<double>(nullable: false),
                    Double = table.Column<int>(nullable: false),
                    GamesPlayed = table.Column<int>(nullable: false),
                    GamesStarted = table.Column<int>(nullable: false),
                    Hits = table.Column<int>(nullable: false),
                    HomeRun = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PlayerNumber = table.Column<int>(nullable: false),
                    Runs = table.Column<int>(nullable: false),
                    RunsBattedIn = table.Column<int>(nullable: false),
                    StolenBase = table.Column<int>(nullable: false),
                    TotalBases = table.Column<int>(nullable: false),
                    Triple = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Year = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerBatting", x => x.BattingID);
                });
            migrationBuilder.CreateTable(
                name: "PlayerFielding",
                columns: table => new
                {
                    FieldingID = table.Column<int>(nullable: false),
                    Assist = table.Column<int>(nullable: false),
                    DoublePlay = table.Column<int>(nullable: false),
                    Error = table.Column<int>(nullable: false),
                    FieldingPercentage = table.Column<double>(nullable: false),
                    PassedBall = table.Column<int>(nullable: false),
                    PlayerName = table.Column<string>(nullable: true),
                    PlayerNumber = table.Column<int>(nullable: false),
                    Putout = table.Column<int>(nullable: false),
                    StolenBallsAttempted = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Year = table.Column<string>(nullable: true),
                    catcher = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerFielding", x => x.FieldingID);
                });
            migrationBuilder.CreateTable(
                name: "PlayerPitching",
                columns: table => new
                {
                    PitchingID = table.Column<int>(nullable: false),
                    AppearanceGamesStarted = table.Column<string>(nullable: true),
                    AtBat = table.Column<int>(nullable: false),
                    BAverage = table.Column<double>(nullable: false),
                    Balk = table.Column<int>(nullable: false),
                    CompleteGame = table.Column<int>(nullable: false),
                    Double = table.Column<int>(nullable: false),
                    EarnedRunAverage = table.Column<double>(nullable: false),
                    EarnedRuns = table.Column<int>(nullable: false),
                    HitByPitch = table.Column<int>(nullable: false),
                    Hold = table.Column<int>(nullable: false),
                    HomeRun = table.Column<int>(nullable: false),
                    InningsPitched = table.Column<double>(nullable: false),
                    PlayerName = table.Column<string>(nullable: true),
                    PlayerNumber = table.Column<int>(nullable: false),
                    Runs = table.Column<int>(nullable: false),
                    SacrificeFly = table.Column<int>(nullable: false),
                    SacrificeHit = table.Column<int>(nullable: false),
                    Save = table.Column<int>(nullable: false),
                    Shutout = table.Column<string>(nullable: true),
                    StrikeOut = table.Column<int>(nullable: false),
                    Triple = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Walks = table.Column<int>(nullable: false),
                    WildPitch = table.Column<int>(nullable: false),
                    Year = table.Column<string>(nullable: true),
                    winsLosses = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerPitching", x => x.PitchingID);
                });
            migrationBuilder.CreateTable(
                name: "Roaster",
                columns: table => new
                {
                    RoasterID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AcademicClass = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    Height = table.Column<double>(nullable: false),
                    Highschool = table.Column<string>(nullable: true),
                    Hometown = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    Weight = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roaster", x => x.RoasterID);
                });
            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityRoleClaim<string>", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityUserClaim<string>", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityUserLogin<string>", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityUserRole<string>", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName");
            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");
            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName");
        }

        /// <summary>
        /// Downs the specified migration builder.
        /// </summary>
        /// <param name="migrationBuilder">The migration builder.</param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("AspNetRoleClaims");
            migrationBuilder.DropTable("AspNetUserClaims");
            migrationBuilder.DropTable("AspNetUserLogins");
            migrationBuilder.DropTable("AspNetUserRoles");
            migrationBuilder.DropTable("GameBatting");
            migrationBuilder.DropTable("GamePitching");
            migrationBuilder.DropTable("Games");
            migrationBuilder.DropTable("Player");
            migrationBuilder.DropTable("PlayerBatting");
            migrationBuilder.DropTable("PlayerFielding");
            migrationBuilder.DropTable("PlayerPitching");
            migrationBuilder.DropTable("Roaster");
            migrationBuilder.DropTable("AspNetRoles");
            migrationBuilder.DropTable("AspNetUsers");
        }
    }
}
